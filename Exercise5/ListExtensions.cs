﻿using System;
using System.Collections.Generic;

namespace Exercise5
{
    public static class ListExtensions
    {
        public static bool CompareTo<T>(this List<T> self, List<T> other)
        {
            Func<List<T>, List<T>, bool> AreSameSet = (a, b) =>
             {
                 // Improve by sliming down this function
                 HashSet<T> ha = new HashSet<T>();
                 HashSet<T> haTMP = new HashSet<T>();
                 a.ForEach(item =>
                 {
                     ha.Add(item);
                     haTMP.Add(item);
                 });
                 HashSet<T> hb = new HashSet<T>();
                 HashSet<T> hbTMP = new HashSet<T>();
                 b.ForEach(item =>
                 {
                     hb.Add(item);
                     hbTMP.Add(item);
                 });

                 hbTMP.ExceptWith(ha);
                 haTMP.ExceptWith(hb);

                 if (hbTMP.Count is 0 && haTMP.Count is 0)
                     return true;
                 return false;

             };
            if ((self is null && other is null) || (self.Count == other.Count && AreSameSet(self, other)))
                return true;
            return false;
        }
    }
}
