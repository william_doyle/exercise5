﻿using System;
using System.Collections.Generic;

namespace Exercise5
{
    class Program
    {
        static void Main(string[] args)
        {
            // 4 -- Test the compareTo extension method...

            Action same = () =>
            {
                Console.BackgroundColor = ConsoleColor.Green;
                Console.WriteLine("same");
                Console.ResetColor();
            };


            Action different = () =>
            {
                Console.BackgroundColor = ConsoleColor.Red;
                Console.WriteLine("different");
                Console.ResetColor();
            };

            { // scope to make potential misspellings more obvious
                string[] arr = { "William", "Doyle", "stroked", "the", "bird" };
                List<string> la = new List<string>();
                List<string> lb = new List<string>();
                foreach (string s in arr)
                    la.Add(s);
                Array.Reverse(arr);
                foreach (string s in arr)
                    lb.Add(s);

                // 4A -- two string lists in different order
                Console.Write($"4A\n\tlen a {la.Count} | len b {lb.Count} | Same or Different?  ");
                if (la.CompareTo(lb))
                    same();
                else
                    different();

                la[1] = "What is a fermion";

                // 4B -- Two different string lists of same length
                Console.Write($"4B\n\tlen a {la.Count} | len b {lb.Count} | Same or Different? ");
                if (la.CompareTo(lb))
                    same();
                else
                    different();

            }
            // 4.C 
            {
                List<int> ia = new List<int>();
                List<int> ib = new List<int>();

                for (int i = 0; i < 7; i++)
                    ia.Add(i >> 1);
                for (int i = 17; i < 93; i += 1)
                    ib.Add(i ^ 2);

                //Console.Write("\nia is ");
                //ia.ForEach(i => Console.Write($"\t{i}")) ;
                //Console.Write("\nib is ");
                //ib.ForEach(i => Console.Write($"\t{i}")) ;

                Console.Write($"4C\n\tlen ia {ia.Count} | len ib {ib.Count} | Same or Different?  ");
                if (ia.CompareTo(ib))
                    same();
                else
                    different();
            }

            // 4.D two null lists
            {
                List<int> null1 = null;
                List<int> null2 = null;
                Console.Write($"4D\n\tSame or Different?");
                if (null1.CompareTo(null2))
                    same();
                else
                    different();

            }
            // 4.E  Two identical lists
            {
                List<double> d1 = new List<double>();
                List<double> d2 = new List<double>();

                for (int i = 0; i < 100; i++)
                {
                    double tmp = 0.001 * i;
                    d1.Add(tmp);
                    d2.Add(tmp);
                }

                Console.Write($"4E\n\tlen d1 {d1.Count} | len d2 {d2.Count} | Same or Different? ");
 if (d1.CompareTo(d2))
                    same();
                else
                    different();

            }







        }
    }
}
